import { ScrollView,Image,Text,StyleSheet, SafeAreaView,View, Button, TextInput } from 'react-native';
import { useEffect, useState } from 'react';
import { Picture } from '../entities';
import { fetchAllPictures } from '../picture-service';
import PictureListItem from '../components/PictureListItem';
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import * as React from 'react';




export default function ConnexionScreen() {
   
        const [username, setUsername] = useState('');
        const [password, setPassword] = useState('');
      
     
  
        return (
    <View style={styles.container}>
         <View >
         <Button  title="Inscription"  color="#17b0a8" />
         <Text></Text>
    <Button  title="Connexion"  color="#17b0a8" />

    <TextInput
        placeholder="Username"
        value={username}
        onChangeText={setUsername}
      />
      <TextInput
        placeholder="Password"
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
    
         </View>
   

    </View>

  );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop:50,
      backgroundColor: '#A2E8DB',
      alignItems: 'center',
      justifyContent: 'center',
      
    },
   
  
  });
