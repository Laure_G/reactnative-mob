import { StyleSheet, View, Text, Button, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import MapView, { Callout } from 'react-native-maps';
import {Marker} from 'react-native-maps';
import { City, Picture } from '../entities';
import { useEffect, useState } from 'react';
import { fetchAllCities, fetchCityPictures } from '../city-service';
import { ReloadInstructions } from 'react-native/Libraries/NewAppScreen';
import { Tooltip } from 'react-native-paper';

export default function MapScreen() {

  const navigation = useNavigation <any>()
  const [city, setCities] = useState<City[]>([]);
  const [pictures, setPictures] = useState<Picture[]>([]);
  const [cityId,setCityId] = useState(1);
  const [region, setRegion] = useState({
    latitude: 45.764043,
    longitude: 4.850000,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });



useEffect(() => {
  fetchCityPictures(1)
      .then(data => setPictures(data))
      .catch(err => console.log(err));
       
}, [1]);




  return (
    <View style={styles.container}>
       <View style={styles.fixToText}>
   <Button color="#17b0a8" title="Lyon" onPress={()=>{ setRegion({
    latitude: 45.764043,
    longitude: 4.840000,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }); setCityId(1);}} ></Button>
   <Button color="#17b0a8"title="Paris" onPress={()=>{  setRegion({
      latitude: 48.866667,
      longitude: 2.333333,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
    }); setCityId(2);}}></Button>
   <Button color="#17b0a8"title="Grenoble" onPress={()=>{ setRegion({
    latitude: 45.1666700,
    longitude: 5.7166700,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }); setCityId(4);}}></Button>
   <Button color="#17b0a8"title="Marseille" onPress={()=>{ setRegion({
    latitude: 43.296482,
    longitude: 5.36978,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  }); setCityId(3);}}></Button>
      </View>
   
      <MapView style={styles.map}  region={region} >
  {pictures && pictures.map(item => (
    <Marker
      key={item.id}
      coordinate={{latitude: item.latitude, longitude: item.longitude}}
      title={item.title}
      pinColor = {"#AB0520"}     
      >
      <Callout style={styles.callout}>
        <Text>{item.title}</Text>
         <Text><Image source={{uri:item.src}} style={{width:200, height:250}}/></Text>
      </Callout>
    </Marker>
    
   
  ))}
    
      </MapView>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:50,
    backgroundColor: '#A2E8DB',
  },
  map: {
    width: '100%',
    height: '100%',
  },
  fixToText: {
    flexDirection: 'row',
    justifyContent: 'space-around',
   paddingBottom: 15,
  },
  callout:{
    justifyContent: 'center',
    alignContent: 'center'
  }

});