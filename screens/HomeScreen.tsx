import { ScrollView,Image,Text,StyleSheet, SafeAreaView, } from 'react-native';
import { useEffect, useState } from 'react';
import { Picture } from '../entities';
import { fetchAllPictures } from '../picture-service';
import PictureListItem from '../components/PictureListItem';
import Icon from "@expo/vector-icons/MaterialCommunityIcons";


export default function HomeScreen() {
  const [pictures, setPictures] = useState<Picture[]>([]);

  useEffect(() => {
    fetchAllPictures()
        .then(data => setPictures(data))
        .catch(err => console.log(err));
}, []);



  return (
    <SafeAreaView style={{ backgroundColor: '#A2E8DB',}} >
    <ScrollView>     
    <Text style={{ textAlign:'center',
    fontSize:40,
    fontFamily: 'sans-serif-condensed',marginTop: 80, fontWeight:'700',
    color:'white' }}>FRAGMENT </Text>
    <Text style={{ textAlign:'center',
    fontSize:20,
    fontFamily: 'sans-serif-condensed',marginBottom:12,
    color:'white',}}>Petits détails pour regards affutés...</Text> 
          <Image source={{uri:"https://images.unsplash.com/photo-1470700467371-d42b6c25c6a8?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MTF8fHBob3RvZ3JhcGhpZSUyMHNtYXJ0cGhvbmUlMjBhcnR8ZW58MHx8MHx8&auto=format&fit=crop&w=800&q=60"}} style={{width:320, height:320,marginHorizontal:20, marginTop:25, marginBottom:25,borderRadius:200 }}/>
       <Text style={{ textAlign:'center',
    fontSize:32,
    fontFamily: 'sans-serif-condensed',margin:10,
    color:'white', }}>What's new?</Text>
    {pictures.map(item => <PictureListItem key={item.id} picture={item} />)}
    
</ScrollView>
</SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#A2E8DB',
    alignItems: 'center',
    justifyContent: 'center'
  },
 
});
