import { Button, Image, Text, View,StyleSheet } from "react-native";
import * as ImagePicker from "expo-image-picker";
import { useEffect, useState } from "react";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";

export default function CameraScreen() {
    const [picked, setPicked] = useState('');
   
    async function pickImage(camera = true) {
        let result;
        if (camera) {
            result = await ImagePicker.launchCameraAsync();

        } else {
            result = await ImagePicker.launchImageLibraryAsync();
        }
        if (result.assets?.length) {
            setPicked(result.assets[0].uri);
        }
    }



    return (
        <View style={styles.container}>
            <Button  title="Prendre une photo"  color="#17b0a8" onPress={() => pickImage()} />
            {picked &&
                <Image source={{ uri: picked }} style={{ width: 100, height: 200,}} />
            }
        </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#A2E8DB',
    alignItems: 'center',
    justifyContent: 'center',
  },

});