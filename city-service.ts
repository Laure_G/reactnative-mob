import axios from "axios";
import { City, Picture } from "./entities";

export async function fetchAllCities() {
  const response = await axios.get<City[]>("http://10.0.21.237:8000/api/city");
  return response.data;
}

export async function fetchOneCity(id: number | string) {
  const response = await axios.get<City>("http://10.0.21.237:8000/api/city" + id);
  return response.data;
}

export async function fetchCityPictures(id: number | string) {
  const response = await axios.get<Picture[]>(`http://10.0.21.237:8000/api/city/${id}/pictures`);
  return response.data;
}