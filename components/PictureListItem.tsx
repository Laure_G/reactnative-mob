import { Button, Image, StyleSheet, Text, View } from "react-native";
import { Picture, User } from "../entities";
import React, { useEffect, useState } from "react";
import { fetchPictureLikes } from "../picture-service";
import { IconButton } from "@react-native-material/core";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import { useNavigation } from '@react-navigation/native';




interface Props {
    picture: Picture;
    
}

export default function PictureListItem({ picture }: Props) {

    const navigation = useNavigation <any>()

    return (
        <View style={styles.container}>
           
    <Text key={picture.id} style={{marginTop: 5,marginLeft:10, marginRight:10}}>{picture.author.pseudo} a photographié "{picture.title}" à {picture.city.name} </Text>
    <Text style={{fontStyle:'italic', marginLeft:10, marginRight:10}}>{(String(picture.date)).slice(0,10)} </Text>
    <Image source={{uri:picture.src}} style={{width:300, height:250,margin:10}}/>
    <Text style={{ marginLeft:10, marginRight:10}}>"{picture.description}..." </Text>

    {((Number(picture.likes?.length) )>0) ? 
    <View style={styles.like}>
       <IconButton 
      icon={props => <Icon name="heart" {...props} />}  
      color="red"/>
      <Text style= {{color:'red',marginLeft: -8 }}>  {picture.likes?.length}</Text>
    </View > :
        <IconButton style={styles.like}
      icon={props => <Icon name="heart-outline" {...props} />}  
      color="red"/>
   }
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        width: '95%',
        backgroundColor: 'white',
        elevation:5,
        borderRadius:10,
        padding: 10,
        margin: 10,
        justifyContent: 'center',
    },
    like: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical:-8,
    },
    
  });
  