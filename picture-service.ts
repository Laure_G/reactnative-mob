import axios from "axios";
import { Picture, User } from "./entities";

export async function fetchAllPictures() {
  const response = await axios.get<Picture[]>("http://10.0.21.237:8000/api/picture");
  return response.data;
}

export async function fetchOnePicture(id: number | string) {
  const response = await axios.get<Picture>("http://10.0.21.237:8000/api/picture" + id);
  return response.data;
}

export async function fetchPictureLikes(id: number | string) {
  const response = await axios.get<User[]>(`http://10.0.21.237:8000/api/picture/${id}/like`);
  return response.data;
}



