export interface Picture {
    id?:number;
    src:string;
    title: string;
    description?:string;
    date:string|Date;
    latitude:number;
    longitude:number;
    author:User;
    likes?:User[];
    city: City;
    category: Category[];
}
export interface City {
    id?:number;
    name:string;
    pictures: Picture[];
}

export interface Category {
    id?:number;
    name:string;
    pictures: Picture[];
}

export interface User {
    id?:number;
    email:string;
    password:string;
    pseudo:string;
    roles: string[];
    pictures?: Picture[];
}