
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import HomeScreen from "./screens/HomeScreen";
import MapScreen from "./screens/MapScreen";
import ProfilScreen from "./screens/ProfilScreen";
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import CameraScreen from "./screens/CameraScreen";
import Icon from "@expo/vector-icons/MaterialCommunityIcons";
import ConnexionScreen from "./screens/ConnexionScreen";




export default function App() {

  const Stack = createNativeStackNavigator();
  const Tab = createMaterialBottomTabNavigator();
  
 
  return (
      <NavigationContainer>
        
          <Tab.Navigator initialRouteName="Home"
  activeColor="#43ded6"
  inactiveColor="#17b0a8"
  shifting
  barStyle={{ backgroundColor: '#e3faf5', height:65 }} 
  >
        <Tab.Screen  name="Actus"  component={HomeScreen} options={{ 
          tabBarIcon: ({ color }) => (
            <Icon name="home" color={color}  size={26} />
          ),
          tabBarColor: 'red'
        }} />
        <Tab.Screen name="Carte" component={MapScreen} options={{
      
          tabBarIcon: ({ color }) => (
            <Icon name="map" color={color} size={26} />
          ),
        }}  />
        <Tab.Screen name="Photo" component={CameraScreen} options={{
         
          tabBarIcon: ({ color }) => (
            <Icon name="camera" color={color} size={26} />
          ),
        }} />
        <Tab.Screen name="Compte" component={ConnexionScreen} options={{
    
          tabBarIcon: ({ color }) => (
            <Icon name="account" color={color} size={26} />
          ),
        }} />
      </Tab.Navigator>
      </NavigationContainer>
  )
}



